/**
 * @description Take a string and make subStrings of a string, then return an object of indexes responsibles for repeated subStrings
 * @param {string} s
 * @param {array} subStringArray
 * @param {number} count
 * @param {object} responsibles
 */
const makeResponsiblesIndexes = (
  s,
  subStringArray = [],
  count = 1,
  responsibles = {}
) => {
  if (count > s.length) return responsibles;

  for (let i = 0; i < s.length; i++) {
    const step = i + count;
    const subString = s.slice(i, step);

    if (subString.length < count) break;

    if (subStringArray.includes(subString)) {
      for (let j = i; j <= step; j++) {
        responsibles[j] = responsibles[j] ? responsibles[j] + 1 : 1;
      }
    }
    subStringArray.push(subString);
  }
  return makeResponsiblesIndexes(s, subStringArray, count + 1, responsibles);
};

/**
 * @description Take an object of key - number values pairs, and return the property that holds the major value.
 * @param {object} responsibles
 */
const getMostResponsible = responsibles => {
  let mostResponsible;
  for (key in responsibles) {
    if (
      !responsibles[mostResponsible] ||
      responsibles[key] > responsibles[mostResponsible]
    ) {
      mostResponsible = Number(key);
    }
  }
  return mostResponsible;
};

/**
 * @description Take a string or array, and return a new iterable of the same type without the element on position "index"
 * @param {iterable} s
 * @param {number} index
 */
const removeIndex = (s, index) => s.slice(0, index) + s.slice(index + 1);

/**
 * @description Take a string and return the minimum deletions that has to be done in order to have non repeated subStrings.
 * @param {string} s
 * @param {number} deletions
 */
let getMinDeletion = (s, deletions = 0) => {
  const responsiblesForRepeating = makeResponsiblesIndexes(s);
  if (Object.keys(responsiblesForRepeating).length) {
    const mostResponsible = getMostResponsible(responsiblesForRepeating);
    const newString = removeIndex(s, mostResponsible);
    return getMinDeletion(newString, deletions + 1);
  }

  return deletions;
};
